import React from 'react';
import AddProductForm from '../components/AddProductForm/AddProductForm';

function AddProduct(props) {
    return (
        <main>
            <section id="addSection">
                <div className="container">
                    <h1>Add product</h1>
                    <AddProductForm />
                </div>
            </section>
        </main>
    );
}

export default AddProduct;
import React from 'react';

function Home(props) {
    return (
        <main>
            <section id="section1">
                <div className="container">
                    <h1>The ultimate shopping experience</h1>
                    <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Fugiat fugit non voluptatem quae reprehenderit facilis voluptatum repellendus suscipit inventore laboriosam eaque, eius veritatis ab esse! Inventore est nulla neque quos?</p>
                </div>
            </section>
        </main>
    );
}

export default Home;
import React from 'react';
import useMainProduct from '../hooks/useMainProduct';
import useProducts from '../hooks/useProducts';
import ProductCard from '../components/ProductCard/ProductCard';
import axios from 'axios';
import { useLoaderData } from 'react-router-dom';

export async function loader() {
    const response = await axios.get(`${import.meta.env.VITE_API_BASE_URL}/products`)
    const products = response.data
    return { products };
}

function Products(props) {

    const { products } = useLoaderData()

    return (
        <main>
            <section>
                <div className="container">
                    <h2>Similar products</h2>
                    <div className="productsList">
                        {
                            products.map(product =>
                                <ProductCard key={product.id} product={product} />
                            )
                        }
                    </div>
                </div>
            </section>
        </main>
    );
}

export default Products;
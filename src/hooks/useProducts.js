import { useState, useEffect } from "react"

function useProducts() {
    const [products, setProducts] = useState([])
    const [error, setError] = useState(null)

    useEffect(() => {
        fetch(`${import.meta.env.VITE_API_BASE_URL}/products`)
            .then(res => res.json())
            .then(json => setProducts(json))
            .catch(error => setError(error))
    }, [])

    return [products, error]
}

export default useProducts
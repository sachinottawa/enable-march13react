import { useState, useEffect } from "react"

function useMainProduct() {
    const [mainProduct, setMainProduct] = useState(null)

    useEffect(() => {
        fetch('https://fakestoreapi.com/products/1')
            .then(res => res.json())
            .then(json => setMainProduct(json))
    }, [])

    return mainProduct
}

export default useMainProduct
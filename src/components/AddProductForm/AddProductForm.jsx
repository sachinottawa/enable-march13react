import axios from "axios"
import { useForm } from "react-hook-form"


export default function AddProductForm() {
    const {
        register,
        handleSubmit,
        watch,
        formState: { errors },
    } = useForm()


    const onSubmit = (data) => {
        const product = {
            ...data,
            category: "65eeff16bdc89d1d2454e9dc"
        }

        axios.post(`${import.meta.env.VITE_API_BASE_URL}/products`, product)
            .then(res => {
                console.log(res.data)
            })
            .catch(error => {
                console.log(error)
            })
    }


    console.log(watch("example")) // watch input value by passing the name of it


    return (
        <form onSubmit={handleSubmit(onSubmit)}>

            <label htmlFor="title">Title</label>
            <input type="text" {...register("title", { required: true })} />
            {errors.title && <span>Title is required</span>}

            <label htmlFor="image">Image</label>
            <input type="text" {...register("image", { required: true })} />
            {errors.image && <span>Image is required</span>}

            <label htmlFor="price">Price</label>
            <input type="number" {...register("price", { required: true })} />
            {errors.price && <span>Price is required</span>}

            <label htmlFor="description">Description</label>
            <textarea name="description" id="description" cols="30" rows="10" {...register("description", { required: true })}></textarea>
            {errors.description && <span>Description is required</span>}

            <input type="submit" />
        </form>
    )
}
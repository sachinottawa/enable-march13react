import { Link } from "react-router-dom";

function Header() {
    return (
        <header>
            <div className="container">
                <a id="logo" href="#">
                    Cartify
                </a>
                <nav>
                    <ul>
                        <li>
                            <Link to="/">Home</Link>
                        </li>
                        <li>
                            <Link to="#">About</Link>
                        </li>
                        <li>
                            <Link to="/products">Products</Link>
                        </li>
                        <li>
                            <Link to="#">Contact</Link>
                        </li>
                    </ul>
                </nav>
                <div className="actionLinks">
                    <a href="#">
                        <span className="material-symbols-outlined">favorite</span>
                    </a>
                    <a href="#">
                        <span className="material-symbols-outlined">local_mall</span>
                    </a>
                </div>
            </div>
        </header>
    )
}

export default Header;
import React from 'react';
import { Link } from 'react-router-dom';

function ProductCard(props) {
    const product = props.product
    console.log(product)
    return (
        <article className="product">
            <img
                src={product.image}
                alt=""
            />
            <div className="productDetails">
                <Link to={"/products/" + product._id}>
                    <h3 className="h6">{product.title}</h3>
                </Link>
                <div>
                    <span>★</span>
                    <span>★</span>
                    <span>★</span>
                    <span>★</span>
                    <span>☆ </span>
                </div>
                <div className="priceAndButton">
                    <span className="p">${product.price}</span>
                    <button className="button buttonPrimary">Add to cart</button>
                </div>
            </div>
        </article>
    );
}

export default ProductCard;
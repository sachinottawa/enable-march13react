import React, { Children } from 'react'
import ReactDOM from 'react-dom/client'
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import Root from './routes/root.jsx';
import SingleProduct, { loader as singleProductLoader } from './routes/singleProduct.jsx';
import ErrorPage from './error-page.jsx';

import App from './App.jsx'
import './index.css'
import Home from './routes/home.jsx';
import Products, { loader as productsLoader } from './routes/products.jsx';
import AddProduct from './routes/add-product.jsx';

const router = createBrowserRouter([
  {
    path: "/",
    element: <Root />,
    errorElement: <ErrorPage />,
    children: [
      {
        path: '/',
        element: <Home />
      },
      {
        path: '/products',
        element: <Products />,
        loader: productsLoader
      },
      {
        path: '/products/:productId',
        element: <SingleProduct />,
        loader: singleProductLoader
      },
      {
        path: '/add-product',
        element: <AddProduct />
      }
    ]
  },

]);

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>,
)
